package com.asadovskaya.lambda;

import com.asadovskaya.lambda.consts.ResultStatus;
import com.asadovskaya.lambda.dto.Post;
import com.asadovskaya.lambda.dto.User;
import com.asadovskaya.lambda.service.AWSClientFactory;
import com.asadovskaya.lambda.service.AWSSimpleSystemManagementAdapter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.zip.GZIPOutputStream;

public class App implements Function<User, String> {

    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private final S3AsyncClient s3AsyncClient;
    private final AWSSimpleSystemManagementAdapter parameterStoreAdapter;

    public App() {
        AWSClientFactory awsClientFactory = AWSClientFactory.getInstance();
        s3AsyncClient = awsClientFactory.getS3AsyncClient();
        parameterStoreAdapter = new AWSSimpleSystemManagementAdapter(awsClientFactory.getSsmAsyncClient());
    }

    @Override
    public String apply(User user) {
        try {
            String bucketName = parameterStoreAdapter.getRemoteValue(System.getenv("BUCKET_NAME"));
            String baseUri = parameterStoreAdapter.getRemoteValue(System.getenv("REST_API_BASE_URL"));

            URI uri = URI.create(baseUri + user.getId() + "/posts");
            HttpClient client = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
            HttpRequest request = HttpRequest.newBuilder()
                    .GET()
                    .uri(uri)
                    .build();

            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            ObjectMapper objectMapper = new ObjectMapper();
            List<Post> posts = objectMapper.readValue(response.body(), new TypeReference<List<Post>>() {
            });
            if (Objects.isNull(posts) || posts.isEmpty()) {
                return ResultStatus.NO_POSTS.name();
            }
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(toZip(response.body()));
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String path = "posts/user=" + user.getId() + "/posts/posts_" + user.getId() + "_" + timestamp + ".json.gz";

            CompletableFuture<PutObjectResponse> responseFuture =
                    s3AsyncClient.putObject(PutObjectRequest.builder()
                                    .bucket(bucketName)
                                    .key(path)
                                    .build(),
                            AsyncRequestBody.fromBytes(byteArrayInputStream.readAllBytes()));

            CompletableFuture<PutObjectResponse> operationCompleteFuture =
                    responseFuture.whenComplete((putObjectResponse, exception) -> {
                        Optional.ofNullable(putObjectResponse)
                                .ifPresentOrElse(
                                        object -> {
                                            logger.trace(object.versionId());
                                        },
                                        () -> {
                                            logger.error(exception.getMessage());
                                        });
                    });

            operationCompleteFuture.join();
        } catch (IOException | InterruptedException | ExecutionException e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }

        return ResultStatus.OK.name();
    }

    private byte[] toZip(String data) throws IOException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        GZIPOutputStream zipStream = new GZIPOutputStream(byteStream);
        zipStream.write(data.getBytes());
        zipStream.close();
        return byteStream.toByteArray();
    }


}
