package com.asadovskaya.lambda.consts;

public enum AWSClientType {
    S3_ASYNC_CLIENT,
    SSM_ASYNC_CLIENT
}