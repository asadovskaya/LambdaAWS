package com.asadovskaya.lambda.service;

import com.asadovskaya.lambda.App;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.services.ssm.SsmAsyncClient;
import software.amazon.awssdk.services.ssm.model.GetParameterRequest;
import software.amazon.awssdk.services.ssm.model.GetParameterResponse;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class AWSSimpleSystemManagementAdapter {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    private final SsmAsyncClient ssmAsyncClient;

    public AWSSimpleSystemManagementAdapter(SsmAsyncClient ssmAsyncClient) {
        this.ssmAsyncClient = ssmAsyncClient;
    }

    public String getRemoteValue(String name) throws ExecutionException, InterruptedException {
        CompletableFuture<GetParameterResponse> responseFuture =
                ssmAsyncClient.getParameter(GetParameterRequest
                        .builder()
                        .name(name)
                        .withDecryption(true)
                        .build());

        CompletableFuture<GetParameterResponse> getCompleteFuture =
                responseFuture.whenComplete((getParameterResponse, exception) -> {
                    Optional.ofNullable(getParameterResponse)
                            .ifPresentOrElse(
                                    response -> {
                                    },
                                    () -> {
                                        logger.error(exception.getMessage());
                                    });
                });

        getCompleteFuture.join();
        return getCompleteFuture.get().parameter().value();
    }
}
