package com.asadovskaya.lambda.service;

import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.ssm.SsmAsyncClient;

public class AWSClientFactory {

    private static volatile AWSClientFactory instance;

    private static S3AsyncClient s3AsyncClient;
    private static SsmAsyncClient ssmAsyncClient;

    private AWSClientFactory() {
        s3AsyncClient = S3AsyncClient
                .builder()
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .build();
        ssmAsyncClient = SsmAsyncClient
                .builder()
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .build();
    }

    public static AWSClientFactory getInstance() {
        AWSClientFactory localInstance = instance;
        if (localInstance == null) {
            synchronized (AWSClientFactory.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new AWSClientFactory();
                }
            }
        }
        return localInstance;
    }

    public static S3AsyncClient getS3AsyncClient() {
        return s3AsyncClient;
    }

    public static SsmAsyncClient getSsmAsyncClient() {
        return ssmAsyncClient;
    }

}
